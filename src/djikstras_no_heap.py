import sys


def next_unvisited(dist, visited):
    """
    Find the next node to visit.  The next node should be the one that has not
    yet been visited and has the minimum distance from the current node.
    """
    min_dist = sys.maxsize
    min_index = -1

    # Search not nearest vertex not in the
    # shortest path tree
    for v in range(len(dist)):
        if not visited[v] and (dist[v] < min_dist or min_index < 0):
            min_dist = dist[v]
            min_index = v

    return min_index


def shortest_distance(graph, src, dest):
    """
    Uses Dijkstra's algorithm to calculate the shortest distance between
    the source and destination in the provided graph.

    Uses a utility function to find the next unvisted node with the minimum
    distance.
    """

    number_of_vertices = len(graph)
    dist = [sys.maxsize] * number_of_vertices
    dist[src] = 0
    visited = [False] * number_of_vertices

    for _ in range(number_of_vertices):

        u = next_unvisited(dist, visited)
        visited[u] = True

        for v in range(number_of_vertices):
            if graph[u][v] > 0 and \
                    not visited[v] and \
                    dist[v] > dist[u] + graph[u][v]:
                dist[v] = dist[u] + graph[u][v]

    return dist[dest]


# Driver program
# g = Graph(9)
# g.graph = [[0, 4, 0, 0, 0, 0, 0, 8, 0],
#            [4, 0, 8, 0, 0, 0, 0, 11, 0],
#            [0, 8, 0, 7, 0, 4, 0, 0, 2],
#            [0, 0, 7, 0, 9, 14, 0, 0, 0],
#            [0, 0, 0, 9, 0, 10, 0, 0, 0],
#            [0, 0, 4, 14, 10, 0, 2, 0, 0],
#            [0, 0, 0, 0, 0, 2, 0, 1, 6],
#            [8, 11, 0, 0, 0, 0, 1, 0, 7],
#            [0, 0, 2, 0, 0, 0, 6, 7, 0]
#            ];


graph = [[0, 3, 4, 0],
         [0, 0, 0.5, 0],
         [0, 0, 0, 1],
         [0, 0, 0, 0]]

print(shortest_distance(graph, 0, 3))

