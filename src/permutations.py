def generate_permutations(elements):
    """
    Recursive implementation of finding the permutations of elements.

    The basic idea is that a permutation of [a, b, c, ... ] is a + permutation of [b, c, ...], b + permutation of
    [a, c, ...], etc.
    """
    if len(elements) <= 1:
        yield elements
    else:
        for perm in generate_permutations(elements[1:]):
            for i in range(len(elements)):
                yield perm[:i] + elements[0:1] + perm[i:]


def print_permutations(elements):
    for perm in generate_permutations(elements):
        print(perm)


print_permutations([1, 2, 3])

