import json


s = '{ "hello": "world", "nested": { "foo": "bar", "arr": [1, 2, 3]} }'
d = json.loads(s)
pretty = json.dumps(d, indent=4)
print(pretty)
