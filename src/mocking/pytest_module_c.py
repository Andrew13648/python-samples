from unittest.mock import patch, Mock
import module_c


def test_f1_no_mock():
    assert module_c.f1() == 'module_b.f1-module_b.f2-module_c.f1'


@patch('module_b.f2')
def test_f1_b_f2_mocked(mock_f2):
    mock_f2.return_value = 'mock f2'
    assert module_c.f1() == 'mock f2-module_c.f1'


@patch('module_b.f1')
def test_f1_b_f1_mocked(mock_f1):
    """Mock only f1 in module b (leave f2 as is)"""
    mock_f1.return_value = 'mock f1'
    assert module_c.f1() == 'mock f1-module_b.f2-module_c.f1'
