"""
This represents a module under test.  Note the invalid import must handled
before testing or else an error will be raised.
"""
from somepackage.foo import Whatever


print('entering module_a')


def f1():
    return 'f1'


def f2():
    return f'{f1()}-f2'


w = Whatever()

print(w)
print(w.f())
print(w.myproperty)
print(w.someproperty)
print(w.someproperty.nestedproperty)
print(w.someproperty.otherproperty.f2().f3())


print('leaving module_a')
