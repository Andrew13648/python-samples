"""
This demonstrates how to mock out a module that may have errors on import.

The imports can't just be patched because patch will attempt to import the
module first, so we have to assign a mock module prior to the import.

Run this file with python directly (`python mock_ex.py`).
"""
import sys
import unittest.mock as mock


m = mock.MagicMock()

m.Whatever().f.return_value = 'f mock'
m.Whatever().myproperty = 'myproperty mock'
m.Whatever().someproperty.nestedproperty = 'nestedproperty mock'
m.Whatever().someproperty.otherproperty.f2().f3.return_value = 'f3 mock'


# Assign a mock to the unresolved package
sys.modules['somepackage.foo'] = m

print('importing...')
# Note that importing the module under test triggers its execution
import module_a
print('finished importing')

module_a.f1()

# Now we can patch the module under test like normal:
with mock.patch('module_a.f1') as mock_f1:
    mock_f1.side_effect = lambda: print('hello from patched f1 mock')
    module_a.f1()


# We can also now use the patch decorator:
@mock.patch('module_a.f1')
def test_f1(mock_f1):
    mock_f1.side_effect = lambda: print('hello from decorator patched f1 mock')
    module_a.f1()


test_f1()
