"""
Examples of using unittest, mock, and patch.

Run using python -m unittest unittest_ex.py.
"""
from unittest import TestCase
from unittest.mock import patch


def a():
    print('inside a')
    return b('from a', key='hello')


def b(s, **kwargs):
    print('inside b', s, kwargs)
    return 111


def fake_b(s, **kwargs):
    print('inside fake b', s, kwargs)
    return 999


class TestExample(TestCase):
    def test_a(self):
        res = a()
        self.assertEqual(111, res)

    # Replace the b function call with a mock
    @patch('unittest_ex.b')
    def test_a_patch(self, b_mock):
        b_mock.return_value = 222
        res = a()
        self.assertEqual(222, res)
        b_mock.assert_called_with('from a', key='hello')
        # Example of how to get the args and kwargs with call_args
        args, kwargs = b_mock.call_args
        print('args:', args)
        self.assertEqual('from a', args[0])
        print('kwargs:', kwargs)
        self.assertTrue('key' in kwargs)
        self.assertEqual('hello', kwargs['key'])

    # Replace the b function call with a fake
    @patch('unittest_ex.b', fake_b)
    def test_a_patch_b_func(self):
        res = a()
        self.assertEqual(999, res)
