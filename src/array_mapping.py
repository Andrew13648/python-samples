"""
Example of mapping 2-d array indices into a 1-d array in both row-major
and column-major ordering.
"""
rows = 4
cols = 6

arr = [i for i in range(rows * cols)]
print(arr)


def element_at_row_major(col, row):
    return arr[cols * row + col]


def element_at_col_major(col, row):
    return arr[rows * col + row]


print("Row major order:")
for i in range(rows):
    for j in range(cols):
        print(element_at_row_major(j, i), " ", end="")
    print()
print("\nColumn major order:")
for i in range(rows):
    for j in range(cols):
        print(element_at_col_major(j, i), " ", end="")
    print()

