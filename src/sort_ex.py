from operator import itemgetter, attrgetter


a = [3, 1, 9, 5, 4, 12, 6, -2, 2]

print('Basic sort:', sorted(a))
b = a.copy()
b.sort()
print('After an in-place sort:', b)
print('Reverse sort:', sorted(a, reverse=True))
print('Sort with a custom function (distance from 5):', sorted(a, key=lambda x: abs(x - 5)))

a = [(1, 2), (3, 1), (2, 4), (5, 9), (-6, 4), (-3, -1), (20, 5), (11, 8)]
print('Sort list of tuples by 1st element:', sorted(a, key=itemgetter(0)))
print('Sort list of tuples by 2nd element:', sorted(a, key=itemgetter(1)))

a = [{'a': 5}, {'a': 9}, {'a': 2}, {'a': 7}, {'a': -3}]
print('Sort a list of dicts with itemgetter:', sorted(a, key=itemgetter('a')))
print('Sort a list of dicts with lambda:', sorted(a, key=lambda x: x['a']))


class MyObject:
    def __init__(self, n):
        self.n = n

    def __repr__(self):
        return str(self.n)


a = [MyObject(5), MyObject(2), MyObject(9), MyObject(4)]
print('Sort a list of objects by attribute:', sorted(a, key=attrgetter('n')))

a = [(1, 1), (2, 1), (1, 2), (2, 2), (3, 1), (1, 3), (3, 2)]
print('Python sorting is stable:', sorted(a, key=itemgetter(0)))
