import functools


# See https://realpython.com/primer-on-python-decorators/ for more info
def decorator(func):
    # functools.wraps will preserve function identity
    # e.g., print(func) will print
    # <function say_hello at 0x0000025D4F9B9598>
    # instead of
    # <function decorator.<locals>.wrapper at 0x000001DEFED99598>
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        print('Before calling', func.__name__)
        ret = func(*args, **kwargs)
        print('After calling', func.__name__)
        return ret
    return wrapper


def repeat(cnt):
    def decorator_repeat(func):
        def wrapper_repeat(*args, **kwargs):
            for _ in range(cnt):
                func(*args, **kwargs)
        return wrapper_repeat
    return decorator_repeat


# The @decorator syntax is the same as:
# say_hello = decorator(say_hello)
@decorator
def say_hello():
    print('Hello')


# Invoking this would be the same as:
# say_hi = decorator(say_hi)(name)
@decorator
def say_hi(name):
    print(f'Hi {name}')


@decorator
def build_hello_str():
    print('Building hello string')
    return 'Hello'


# The @repeat syntax is the same as:
# say_hey_twice = repeat(2)(say_hey_twice)
@repeat(2)
def say_hey_twice():
    print('Hey')


say_hello()

say_hi('Harry')

print(build_hello_str())

say_hey_twice()

# Demo of functools.wraps
print(say_hello)
