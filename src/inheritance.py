"""
Simple examples of how classes and inheritance work.
"""


class Demo:
    # x is defined as a class variable
    x = 5

    def __init__(self):
        # y is defined as an instance variable
        self.y = 6


class Demo2(Demo):
    a = 7

    def __init__(self):
        super().__init__()
        self.b = 8


d = Demo2()
d.z = 9
print(d)
print(d.__dict__)  # Prints {'y': 6, 'b': 8, 'z': 9} - only instance variables
print(d.x)
print(d.a)
d.x = 10  # This is actually assigning x as an instance variable
print("x =", d.x)  # Prints x = 10
print("x =", d.__class__.x)  # Still prints x = 5
print("y =", d.y)  # Prints 6
print(d.__class__)
print("a =", d.__class__.a)  # Prints a = 7
print("a =", Demo2.a)  # Also prints a = 7
Demo2.a = 8
print("a = ", d.__class__.a)  # Prints 8
print(d.__class__.__dict__)
