"""
Example of checking if the stdin is a TTY and reading user input.
"""
import sys
import getpass


if not sys.stdin.isatty():
    print("stdin is not a TTY; prefix with winpty if running Git Bash")
    exit(1)
s = input("Enter string to echo: ")
print(s)
s = getpass.getpass("Enter hidden string to echo: ")
print(s)
