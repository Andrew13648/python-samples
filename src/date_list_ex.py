"""
Demonstrates how to generate a list of date strings between a start date and an
end date using a generator function.
"""
from datetime import date, timedelta


def generate_dates(start_date, end_date):
    while start_date <= end_date:
        yield start_date.strftime('%Y-%m-%d')
        start_date += timedelta(days=1)


date_list = generate_dates(date(2021, 11, 1), date(2021, 11, 5))
print(list(date_list))
