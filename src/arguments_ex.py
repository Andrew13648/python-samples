"""
Demonstrates how to get program arguments.
"""
import sys
import argparse


# Simple way is to just use sys.argv:
print(sys.argv)

# argparse can handle more complex parsing:
parser = argparse.ArgumentParser()
# Add a positional, required argument:
parser.add_argument('arg1')
# Allow for an optional argument (indicated by -- and -):
parser.add_argument('--optional', '-o')
# Optional argument with a default:
parser.add_argument('--foo', '-f', default='bar')
# Optional boolean argument - only in Python 3.9
# parser.add_argument('--boolean', '-b', default=False, action=argparse.BooleanOptionalAction)
args = parser.parse_args()
print(args)
print(args.arg1)
# Run python arguments_ex.py -h for help
