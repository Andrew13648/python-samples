"""
Demonstrates how to access the current Python version at runtime.
"""
import sys

v = sys.version_info
print(v)
print(v.major)
print(f'{v.major}.{v.minor}.{v.micro}')
