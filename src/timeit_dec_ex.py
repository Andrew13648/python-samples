"""
Example of using a decorator in conjunction with timeit for measuring
performance.  See also timeit_ex.py and decorators_ex.py.
"""
from timeit import default_timer as timer
import time


def print_elapsed_time(func):
    def wrapper(*args, **kwargs):
        start = timer()
        value = func(*args, **kwargs)
        end = timer()
        elapsed = end - start
        print(f'Elapsed time for {func.__name__!r} was {elapsed:.4f} seconds')
        return value
    return wrapper


@print_elapsed_time
def sample():
    print('Inside sample')
    time.sleep(1)


sample()
