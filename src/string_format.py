
# Format integers
n = 2939
# Format with commas
print(f'{n:,d}')
# Pad with 0's
print(f'{n:05d}')
# Right aligned
print(f'{n:10d}')
# Center aligned
print(f'{n:^10d}')
# Left aligned
print(f'{n:<10d}here')
# Pad with .'s
print(f'{n:.<10d}here')

# Format floating point numbers
print(f'{82832.138:,.2f}')
# Format exponent
print(f'{193921.481:.2e}')
# Format with zero
print(f'{.138:0.2f}')
# Format percentage
print(f'{.138:.2%}')
