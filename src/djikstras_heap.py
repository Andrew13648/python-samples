import sys
import heapq


def shortest_distance(graph, src, dest):
    """
    Uses Dijkstra's algorithm to calculate the shortest distance between
    the source and destination in the provided graph.

    Uses a minheap to find the next vertex to be processed.
    Expects the graph as an adjacency matrix.
    """

    number_of_vertices = len(graph)
    dist = [sys.maxsize] * number_of_vertices
    dist[src] = 0
    visited = [False] * number_of_vertices
    heap = [(0, src)]

    while heap:

        _, u = heapq.heappop(heap)
        visited[u] = True

        for v in range(number_of_vertices):
            if graph[u][v] > 0 and \
                    not visited[v] and \
                    dist[v] > dist[u] + graph[u][v]:
                dist[v] = dist[u] + graph[u][v]
                heapq.heappush(heap, (dist[v], v))

    return dist[dest]


graph = [[0, 4, 0, 0, 0, 0, 0, 8, 0],
         [4, 0, 8, 0, 0, 0, 0, 11, 0],
         [0, 8, 0, 7, 0, 4, 0, 0, 2],
         [0, 0, 7, 0, 9, 14, 0, 0, 0],
         [0, 0, 0, 9, 0, 10, 0, 0, 0],
         [0, 0, 4, 14, 10, 0, 2, 0, 0],
         [0, 0, 0, 0, 0, 2, 0, 1, 6],
         [8, 11, 0, 0, 0, 0, 1, 0, 7],
         [0, 0, 2, 0, 0, 0, 6, 7, 0]
         ]
print(shortest_distance(graph, 0, 3))  # 19
print(shortest_distance(graph, 0, 5))  # 11
print(shortest_distance(graph, 0, 8))  # 14

# dist = 4.5
graph = [[0, 3, 4, 0],
         [0, 0, 0.5, 0],
         [0, 0, 0, 1],
         [0, 0, 0, 0]]

print(shortest_distance(graph, 0, 3))  # 4.5
