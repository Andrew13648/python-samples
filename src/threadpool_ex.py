"""
This example launches multiple threads using a ThreadPoolExecutor and passes
values to each thread.
"""
from concurrent.futures import ThreadPoolExecutor
import time
import threading


def thread_function(val):
    print(threading.get_ident())
    print(f"Thread {threading.get_ident()} received {val}")
    time.sleep(1)
    print(f"Thread {threading.get_ident()} is exiting thread_function")


with ThreadPoolExecutor(max_workers=3) as executor:
    executor.map(thread_function, range(4))

print("Execution complete; exiting")
