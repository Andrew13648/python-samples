import base64


unencoded = 'hello world'
print(base64.b64encode(unencoded.encode('utf-8')))

encoded = 'aGVsbG8gd29ybGQ='
print(base64.b64decode(encoded).decode('utf-8'))
