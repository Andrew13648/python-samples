import json
import jsonschema


# This schema file was generated from employee.proto using protobuf-jsonschema
with open("jsonschema_ex.json") as f:
    schema = json.load(f)
    schema = schema["definitions"]["sample_package.Employee"]
    # Note that protobuf-jsonschema does not generate this on the output
    schema["additionalProperties"] = False

print(schema)

# Validates successfully
d = {"name": "John", "id": 289, "salary": 50000, "skills": ["python", "docker"]}
jsonschema.validate(instance=d, schema=schema)

# Extra properties trigger a failure if additionalProperties = False
d = {"name": "John", "a": "b"}
try:
    jsonschema.validate(instance=d, schema=schema)
except Exception as e:
    print(e)

# Raises an exception
d = {"name": "John", "id": "Invalid"}
jsonschema.validate(instance=d, schema=schema)
