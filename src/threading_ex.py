"""
Simple example of starting a single thread.
"""
import threading
import time


def thread_function(val):
    print(f"Thread {threading.get_ident()} received {val}")
    time.sleep(1)
    print(f"Thread {threading.get_ident()} is exiting thread_function")


thread = threading.Thread(target=thread_function, args=(1,))

print("starting thread")
thread.start()
print("finished starting thread")

thread.join()
print("Thread complete")
