"""
Use Differ to compare multi-line strings.
"""
from difflib import Differ

s1 = 'differ compares\nmulti-line strings\nvery well'.splitlines()
s2 = 'differ compares\nmulti-line strings\nreally well.'.splitlines()

result = list(Differ().compare(s1, s2))
for line in result:
    print(line)

print('-' * 80)

s1 = 'differ also compares one line strings'.splitlines()
s2 = 'differ also compares 1 line strings'.splitlines()

result = list(Differ().compare(s1, s2))
for line in result:
    print(line)
