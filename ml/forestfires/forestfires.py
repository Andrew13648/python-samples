import pandas as pd


filename='forestfires.csv'
names = ['X', 'Y', 'month', 'day', 'FFMC', 'DMC',
         'DC', 'ISI', 'temp', 'RH', 'wind', 'rain', 'area']
df = pd.read_csv(filename, names=names)
print(pd.isnull(df))
print(pd.isna(df))

# These options control how pandas prints the output to the console
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
# pd.set_option('display.width', 150)

# Print the first row, all columns
print(df.loc[0, :])

print(df.shape)

# This will filter out rows where there is one value that is NaN
# Try using foresfires2.csv for an example
# df = df[df.isna().any(axis=1)]
# print(df)

print(df.dtypes)

print(df.head(1))
print(df.describe())
print(df.corr())
# pearson is the default method
print(df.corr(method='pearson'))
