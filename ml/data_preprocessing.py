import pandas as pd
from sklearn.preprocessing import Imputer, LabelEncoder, OneHotEncoder


dataset = pd.read_csv('Data.csv')

# x is the independent variable.  Take all rows but not last col.
x = dataset.iloc[:, :-1].values
# y is the dependent variables
y = dataset.iloc[:, 3].values

print(x)
print(y)

# Replace all NaN values (Germany and Spain) with the mean value of the columns
imputer = Imputer(missing_values='NaN', strategy='mean', axis=0)

# Fit the imputer only to columns 2 and 3
# imputer = imputer.fit(x[:, 1:3])
# x[:, 1:3] = imputer.transform(x[:, 1:3])

# Note you can also use fit_transform for only one call:
x[:, 1:3] = imputer.fit_transform(x[:, 1:3])
print(x)

# This code will encode each column to a numeric value (France = 0, Germany = 1, Spain = 2, etc.)
# This isn't right because there isn't a numeric relationship between these
# In other words, Germany isn't greater than France although 1 > 0
labelencoder_x = LabelEncoder()
enc = labelencoder_x.fit_transform(x[:, 0])
print(enc)
x[:, 0] = enc
print(x)

# In order to ensure the ML models don't attribute an order to the values, use one hot encoding
ohe = OneHotEncoder(categorical_features=[0])
x = ohe.fit_transform(x).toarray()
print(x)

labelencoder_y = LabelEncoder()
enc = labelencoder_y.fit_transform(y)
print(enc)
print(y)

