import boto3
from pprint import pprint


dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('sample-table')

response = table.get_item(
    Key={
        'firstName': 'Andrew'
    }
)

pprint(response)
if 'Item' in response:
    pprint(response['Item'])
else:
    print('Item not found')
