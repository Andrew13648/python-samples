import boto3
import random
from pprint import pprint


dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('sample-table')

response = table.update_item(
    Key={
        'firstName': 'Andrew'
    },
    UpdateExpression='set v2 = :v',
    ExpressionAttributeValues={
        ':v': random.randint(1, 1000000)
    },
    # Only update the item if it already exists
    ConditionExpression='attribute_exists(firstName)',
    # Specify what to return after the update
    ReturnValues='ALL_NEW'
)

pprint(response)
pprint(response['Attributes'])
