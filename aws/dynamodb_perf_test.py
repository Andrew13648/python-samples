import boto3
import random
import string


dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('sample-table')

for _ in range(100):
    name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
    response = table.put_item(
        Item={
            'firstName': name,
            'status': 'in progress',
            'v1': random.randint(1, 1000000),
            'v2': random.randint(1, 1000000)
        }
    )
    print('put_item', name)
